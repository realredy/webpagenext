import { generateText, revertFriendlyUrl } from "../utils/shortText";


it('testtin nrar', ()=>{
    const innertest = 'Bacon ipsum dolor amet aliqua qui labore pariatur. Filet mignon et kevin alcatra chicken brisket aute andouille qui eiusmod veniam commodo. Aliqua kielbasa esse ground round drumstick. Beef ribs dolore fatback picanha chislic landjaeger nostrud pastrami biltong. Quis eiusmod cupim, eu turkey nostrud chislic.'
    const text = generateText(innertest)
    expect(text).toContain('Bacon ipsum dolor amet aliqua qui labore pariatur. Filet mignon et kevin alcatra chicken briske...')
})

it('realizado test de url son guion', ()=>{
    const textGuion = 'my-url-con-quion'
    expect(revertFriendlyUrl(textGuion)).toEqual('my url con quion') 
})