export const fulldata = {
  sliders: [
    {
      id: 0,
      type: "sliders",
      title: "Maquetador",
      subtitle: `Rapidéz en la ejecución y adaptabilidad pixel perfect.`,
      text: "Con los últimas tendencias de maquetación y el correcto uso de las etiquetas en posiciones que ayudan a los mototes de búsqueda a indexar el contenido el web site de manera eficaz, así como el correcto uso de las últimas tecnologias de maquetación tal como el uso de -flexbox- y estilos dedicados a cada navegador, son los conocimientos aplicados y orientados a la adaptabilidad de los diferentes dispositivos moviles. A esto combinado con más de 5+ años de experiencia continua en donde utilizo técnicas de planeación y con esto customizar el resultado final y por ende acortando el tiempo empleado en producción.",
    },
    {
      id: 1,
      type: "sliders",
      title: "JavaScript",
      subtitle: `Suficiente experiencia como para poder adaptarme a cualquier framework.`,
      text: "Han pasado ya más de 5 años que conocí la librería de JQuery que en nada es una librería decadente, más, no paso mucho tiempo cuando reconocí que trabajar directamente con javascript me abriría las puertas para poder adaptarme fácilmente a librerías como angular que en mi caso la utilicé en ionic, como actualmente es el día a día al programar con reactJS Tal vez difieras en mi opinión, pero 5 años de experiencia trabajando con javascript, me da el poder desarrollar con cualquier librería con base javascript, me basta con leer la documentación para poder ejecutar tareas con la misma por ende, el dar un paso adelante para conocer javascript me abre las puertas a las demás tecnologías cosa que no hubiera sido posible solo si me hubiese quedado con jQuery.",
    },
    {
      id: 2,
      type: "sliders",
      title: "PhP y MySQL",
      subtitle: `5+ años de experiencia ...queda mucho que aprender ...pues si!`,
      text: "En parte he creado de todo con php, implementando código simples o complejos, crud, secctions depende del proyecto aplicar principios SOLID, usos de clases, funciones publicas, privadas, protected ect... En la actualidad ya no tan fan de MySQL luego de probar FIREBASE, pero de igual manera todo depende del proyecto que se realizará. En tanto a la complejidad y estructura, no tengo problemas en crear una planificación de proyecto en donde se crearían tablas relacionadas para obtener datos conjuntos, endpoint, ect... No puedo definir a que nivel puedo colocarme en tanto conocimiento, pero he resuelto cada reto sin mayor preocupación.",
    },
    {
      id: 3,
      type: "sliders",
      title: "Wordpress",
      subtitle: `No soy un instalador de pluguins! Sin animos de ofender. Esto es de novatos.`,
      text: "He visto ya bastantes perfiles de personas que dicen que !soy programador en wordpress!, Pues No!. No eres programador de wordpress si no sabes de PHP, por ende luego de conocer cómo se conforma la estructúra de tablas de wordpress y de cómo se comunican y se relacionan así de como va las estructúras de las funciones propias de wordpress, lo siguiente es decir, que puedo crear cualquier proyecto en donde se requiera crear un theme, plugin ect... hasta generar customizaciones importantes a medida, y con esto garantizar el control total y una velocidad de carga óptima, menos problemas por actualizaciones y muy poca dependencia de código de terceros. En definitiva puedes probarme y colocarme en un reglón como experto en wordpress o que se necesita para serlo, en todo caso siempre estoy abierto a críticas siempre que vengan de la mano de un conocimiento que acompañe a la misma.",
    },
    {
      id: 4,
      type: "sliders",
      title: "Pack Adobe",
      subtitle: `Desde 1998... Todo inicio con photoshop.`,
      text: "Photoshop, ilustrator, indesign, corel ect.. Con una carrera como publicista con especialidad en diseño gráfico, cursando materias como: dibujo profesional, pintura, escultura, fotografía, producción, para entonces no entendía para que necesitaba todo estos conocimientos si en fin estaba orientado al mundo del diseño digital y no al artístico. Todo esto es simple de explicar, no puedes ser un buen diseñador gráfico si no tienes creatividad ya que sea a través de un medio digital o plasmando un lienzo, todo está relacionado con el mundo artístico y la creatividad. Retornando, ser developer y ser un creativo son dos ramas que pocos dominan al mismo tiempo y es precisamente esto lo que me hace destacar, el poder dominar las herramientas del diseño digital con una base y experiencia combinado con el mundo de la programación.",
    },
    {
      id: 5,
      type: "sliders",
      title: "ReactJS",
      subtitle: `ReactJS para bien o para mal, es el que he elegido.`,
      text: "Mientras otros dirían porque reactJS, debes optar por GO, Angular, (Vue ya que estás relacionado con php), o tal vez, Node o Python, al final creo que con los avances que ha tenido además de que la curva de aprendizaje será más corta, el proceso de pasar a react native la cual es mi meta con todo el poder de combinarlo con llamadas a apis, firebase y tal, el cual está bastante completo como para crear web, app modernas y demas, intentaré programar este portal sacando el máximo de potencial que obtendré al combinarlo con firebase. Y que mejor muestra de migrar mi web a react para predicar con el ejemplo por lo tanto si quieres tener una idea de mis conocimientos inicia con dejar tus comentarios aquí tal vez me des una idea de como mejorarlo.",
    },
    {
      id: 6,
      type: "sliders",
      title: "webpack",
      subtitle: `WebPack es un conocimiento que no puede faltar`,
      text: "Como base de todos los frameworks compuestos por webpack, este conocimiento no puede quedar desapersivido, ya que es la raíz de los FM tales como VUE, ANGULAR, IONIC, entonces ir tras este conocimiento es algo que no puede faltar, y debido a esto, es que puedo con una conciencia sólida enfrentarme a desarrollar conociendo que esta haciendo el software para generar mi app. Dicho esto, puedo combinar webpack con todos sus módulos tales como babel, scss, minificadores ect, para desarrollar con wordpress sitios web extremadadamente rápidos y modulares con la últimas tendencias del desarrollo moderno.",
    },
  ],
  sliders_en: [
    {
      id: 0,
      type: "sliders",
      title: "Layout maker",
      subtitle: `Speed in executing and adaptability on pixel perfect`,
      text: `Speed in executing and adaptability on pixel perfect`,
      text: `Whit ultimate tendency of layout in the proper use of label in position that help at search engines to index
    web content on efficient wey, as well as correct use of last technologies of layout design such as -flexbox- and styles dedicated
    at each browser, that is the applied knowledges and oriented tp adaptability of diferent mobile devices.<br /><br />
    To this, combined whit more of five year old of cotinued experience where I use techniques of planning for finaly
    customize get a great results.`,
    },
    {
      id: 1,
      type: "sliders",
      title: "JavaScript",
      subtitle: `Suficiente experiencia como para poder adaptarme a cualquier framework.`,
      text: `have passed, more of five years, that I knew the framework jquery, but not passed so much time when recognized if I work 
    directly whit javascript, this can open  new doors at knowledges bigger like angular, reactJS, no-relationship databases, 
    such as nextJS.<br><br> At this moment, I have more of five years work with different frameworks, for this, when need a new knowledge, 
    only I read her documentation, and is easy to implement and use this new information, maybe this will not possible if I only keep whit jquery`,
    },
    {
      id: 2,
      type: "sliders",
      title: "PhP y MySQL",
      subtitle: `Five years experience ...there is much to learn, is the right`,
      text: `I have build so much stuff in php like implement code for build theme wordpress, joomla ect... CRUD, drive knowledge for apply 
  user session including solid methodology and more, concepts MVC, function and class, PDO ect..<br><br>
  Is to my familiar use composer for install dependencies, or use php for build API rest Services, including security parameter in projects
  before haved implement library like strype and paypal, or send mail whit SPMT and more. at the end, from build a landing page or make a theme 
  for wordpress, I'm maked a lot of project use php `,
    },
    {
      id: 3,
      type: "sliders",
      title: "Wordpress",
      subtitle: `I'm not a plugin installer, I'm so much more than this.`,
      text: `I have seen a lot of profiles, those say, I'm wp developer, but exist several skills for say this, because is not sufficient add a menu or 
    install a theme, is necesary know about Database, Javascript, php. Is higly mantadory get knowled whit the last tendencys to create actualized web 
    site in wordpress, you haved got skil about webpack, node, AFC, make Plugin, maybe simple but know how build this ect..<br><br>
    In this aspect my profile is diferent because my experience I got was got all this knowledges to make a full project by myselft including, planing 
    time, Grapic design, frontEnd, Backend ect`,
    },
    {
      id: 4,
      type: "sliders",
      title: "Pack Adobe",
      subtitle: `Since 1998... All init whit photoshop.`,
      text: `photoshop, Ilustrator, corel, whit a carrer like degree in grapic design, where I got knowledge in fonts, color theory, photograpic, 
    composition elements, Digital campaigning for audiovisual media.
    while I was study, not know why I need all this knowledges and how can help me in my carrer?<br /> 
    at the end, in my first work, all this knowledge , make my profile have more than know a software, have creativity before make a design.<br>
    I use this creativity in all aspect of design and codig, where i can offer a product whit quality in design and good code.`,
    },
    {
      id: 5,
      type: "sliders",
      title: "ReactJS",
      subtitle: `ReactJS is for good or bad, the framework I was selected`,
      text: `While other say me, why go to use React instead Angular or Vue if is more familiar for you and realy, I used Angular for build the app 
  that show below in works slider. after investigating about react and react native, and how easy is learn this framework whit all your potential, 
  I decided is the way to follow like front developer.   
  Apart of this, can use my knowledge in javascript and pass after build app for android, including convert to ios. 
  
  your sistem of recurcivity, hooks, and all tools for use different database, library of npm or yarn. thought other maybe will be for me more difficult, 
  prefer for this go for in continue grow join my after skills`,
    },
    {
      id: 6,
      type: "sliders",
      title: "webpack",
      subtitle: `webpack is a skill, can't lack`,
      text: `Used by frameworks such as React or Angular even in the development of well-known applications such as Twitter, Instagram,
  PayPal or the web version of WhatsApp. Knowing how to use this module packer is extremely important to keep up with
  current advances in web development, not so much because it is present in most frameworks, but because of the powerful advantage it has
  using it.<br><br>
  CSS minifiers, sass compilers, development dependencies, linters, there are few tools that cannot be missing at all
  project at present therefore not knowing about webpack creates a great void in those who are currently developing.`,
    },
  ],
  works: [
    {
      id: 0,
      type: "works",
      img: "/base-mockup.jpg",
      alt: "ricardo lavour landing page",
      base: "wordpress",
      integ: "Strype - Paypal - TagManager - Materialize",
      desc: "LandingPage desarrollado con wp, totalmente personalizado. En este proyecto se integrado tanto strype como paypal de manera integrada por lo que todo el proceso es realizado sin redirecciones. Dentro de las cualidades de este portal está la capacidad de poder insertar información dentro de la base de datos en diferentes momentos del proceso, como en diferentes secciones y con procesos tán complejos como: insertar multimedia, crear post, insertar múltiples post_meta, crear usuarios. Otra capacidad es la de envio de emails autumatizados, además de acceder a los datos suministrados previamente mediante un sistema de login. Theme 100% programado desde cero, 100% responciv.",
    },
    {
      id: 1,
      type: "works",
      img: "/base-mockup2.jpg",
      alt: "ricardo lavour maquetación",
      base: "HTML - CSS - JavaScript",
      integ: "WoW.js - Materialize",
      desc: "En este portal, sencillo o de finalidad informativa, ( en este solo participo realizando el maquetado y la funcionalidad ), quedando el concepto de diseño en mano de un tercero. Solo me compete mecionar el tiempo de desarrollo, 7 días x 4 horas. Y es en lo que destaco, en la manera o técnicas que implemento para desarrollar sitios en corto tiempo, claro una vez tengo todo a la mano como diseño, imágenes y recursos.",
    },
    {
      id: 2,
      type: "works",
      img: "/mobileApp.mp4",
      alt: "ricardo lavour mobile app",
      base: "Angular - Ionic:3",
      integ: "Materialize",
      desc: "Esta all (showroom) creada con la finalidad de mostar los articulos de un woocomerce. Alimentada de una api creada con php la cual genera un JSON, cuenta con la posibilidad de compartir el contenido en redes sociales. y su propio backend para agregar nuevos productos, editar y eliminar.",
    },
    {
      id: 3,
      type: "works",
      img: "/base_mockup_2.jpg",
      alt: "ricardo lavour Elementor web design",
      base: "Wordpress",
      integ: "PHP - Elementor - Javascript",
      desc: "Web site con alto sentido del diseño, colores, íconos, control de espacios creada totalmente con elementor pero con algunas implementaciones de javascript para darle funcionalidad a algunas secciones.",
    },
    {
      id: 4,
      type: "works",
      img: "/landing_page.jpg",
      alt: "ricardo lavour formulario",
      base: "PHP - MySQL",
      integ: "Semantic UI",
      desc: " Este landingpage o formulario, desarrollado 100% con php-javascript-materialUI con más de 4 capas de seguridad, total seguimiento de las KeyCaps para controlar los datos suministrados y a la vez evitar los bots, envio de datos hacia el c*** para la cual se suma otra integración extra.",
    },
  ],
  works_en: [
    {
      id: 0,
      type: "works",
      img: "/base-mockup.jpg",
      alt: "ricardo lavour landing page",
      base: "wordpress",
      integ: "Strype - Paypal - TagManager - Materialize",
      desc: `landing Page developed with wordpress, totally personalized. 
  on this project was integrated paypal and strype inside for this aspect 
  all the process is realized inside of page. This project has the ability 
  of write data from the frontend of page like add media, user, post and 
  send email automatically when finish the process fill of all field, at 
  same time filter this data send previously`,
    },
    {
      id: 1,
      type: "works",
      img: "/base-mockup2.jpg",
      alt: "ricardo lavour maquetación",
      base: "HTML - CSS - JavaScript",
      integ: "WoW.js - Materialize",
      desc: `This simple web site, made with purpose, informative. 'I only did 
  make code, no design' where put only 7 days, 4 hour by day for finish 
  totally this project, I  implement php, wow.js and materialize.js, to do 
  that, implement techniques of pre-planing then can get measure time to finish all the project`,
    },
    {
      id: 2,
      type: "works",
      img: "/mobileApp.mp4",
      alt: "ricardo lavour mobile app",
      base: "Angular - Ionic:3",
      integ: "Materialize",
      desc: `This showroom build to show items from a woocomerce made with wordpress, this app use a api 
  rest service build with php, has your oun backend system to add, edit and delete items, 
  has ability to share items to social media. This app was my first contact with angular-ionic
  but equal made by myself, I was  implemented materialize and another tools to get this results`,
    },
    {
      id: 3,
      type: "works",
      img: "/base_mockup_2.jpg",
      alt: "ricardo lavour Elementor web design",
      base: "Wordpress",
      integ: "PHP - Elementor - Javascript",
      desc: `web site with highly sense of design, color, elemts, control of spaces, made with elementor, 
  wordpress, but have great functions with javascript `,
    },
    {
      id: 4,
      type: "works",
      img: "/landing_page.jpg",
      alt: "ricardo lavour formulario",
      base: "PHP - MySQL",
      integ: "Semantic UI",
      desc: `Ttis form with the finality to get customers, made 100% php-javascript, highly security 
  keycap detect bad write that can damage the system, has conected to send data at crm, Semantic UI 
  integrations and more.`,
    },
  ],
  topdata: [
    {
      selector: `selected_JavaScript`,
      title: `Suficiente experiencia como para poder adaptarme a cualquier framework`,
      text: `Han pasado ya más de 5 años que conocí la librería de JQuery que en nada es 
          una librería decadente, más, no paso mucho tiempo cuando reconocí que trabajar directamente con 
            javascript me abriría las puertas para poder adaptarme fácilmente a librerías como angular que 
            en mi caso la utilicé en ionic, como actualmente es el día a día al programar con reactJS<br/>
            Tal vez difieras en mi opinión, pero 5 años de experiencia trabajando con javascript, me da el poder 
            desarrollar con cualquier librería con base javascript, me basta con leer la documentación para poder ejecutar tareas con la misma
            por ende, el dar un paso adelante para conocer javascript me abre las puertas a las demás tecnologías
            cosa que no hubiera sido posible solo si me hubiese quedado con jQuery.`,
    },
    {
      selector: `selected_Maquetador`,
      title: `Rapidéz en la ejecución y adaptabilidad pixel perfect`,
      text: `Con los últimas tendencias de maquetación y el correcto uso de las 
                etiquetas en posiciones que ayudan a los mototes de búsqueda a indexar el contenido el web
                site de manera eficaz, así como el correcto uso de las últimas tecnologias de maquetación
                tal como el uso de -flexbox- y estilos dedicados a cada navegador, son los conocimientos 
                aplicados y orientados a la adaptabilidad de los diferentes dispositivos moviles.<br /> 
                A esto combinado con más de 5+ años de experiencia continua en donde utilizo técnicas de 
                planeación y con esto customizar el resultado final y por ende acortando el tiempo
                empleado en producción.`,
    },
    {
      selector: `selected_phpmysql`,
      title: `5+ años de experiencia ...queda mucho que aprender ...pues si!`,
      text: `En parte he creado de todo con php, implementando código simples o complejos, crud, secctions
                depende del proyecto aplicar principios SOLID, usos de clases, funciones publicas, privadas, protected ect...<br>
                 En la actualidad ya no tan fan de MySQL luego de probar FIREBASE, pero de igual manera todo 
                depende del proyecto que se realizará.<br> En tanto a la complejidad y estructura, no tengo problemas en 
                crear una planificación de proyecto en donde se crearían tablas relacionadas para obtener datos conjuntos,
                endpoint, ect...<br>
                No puedo definir a que nivel puedo colocarme en tanto conocimiento, pero he resuelto cada reto sin mayor 
                preocupación.`,
    },
    {
      selector: `selected_Wordpress`,
      title: `No soy un instalador de pluguins! Sin animos de ofender. Esto es de novatos.`,
      text: `He visto ya bastantes perfiles de personas que dicen que !soy programador en wordpress!,
                Pues No!. No eres programador de wordpress si no sabes de PHP, por ende luego de conocer cómo se conforma la estructúra
                de tablas de wordpress y de cómo se comunican y se relacionan así de como va las estructúras de las funciones 
                propias de wordpress, lo siguiente es decir, que puedo crear cualquier proyecto en donde se requiera crear un theme, 
                plugin ect... hasta generar customizaciones importantes a medida, y con esto garantizar el control total y
                 una velocidad de carga óptima, menos problemas por actualizaciones y muy poca dependencia de código de terceros.
                 En definitiva puedes probarme y colocarme en un reglón como experto en wordpress o que se necesita para serlo, 
                 en todo caso siempre estoy abierto a críticas siempre que vengan de la mano de un conocimiento que acompañe a la misma.`,
    },
    {
      selector: `selected_adobe`,
      title: `Desde 1998... Todo inicio con photoshop.`,
      text: `Photoshop, ilustrator, indesign, corel ect.. Con una carrera como publicista con especialidad
                en diseño gráfico, cursando materias como: dibujo profesional, pintura, escultura, fotografía, producción, para entonces no 
                entendía para que necesitaba todo estos conocimientos si en fin estaba orientado al mundo del diseño digital y no al artístico. 
                Todo esto es simple de explicar, no puedes ser un buen diseñador gráfico si no tienes creatividad ya que sea a través de un medio digital o 
                plasmando un lienzo, todo está relacionado con el mundo artístico y la creatividad. Retornando, ser developer y ser un creativo son
                dos ramas que pocos dominan al mismo tiempo y es precisamente esto lo que me hace destacar, el poder dominar las herramientas
                del diseño digital con una base y experiencia combinado con el mundo de la programación.`,
    },
    {
      selector: `selected_react`,
      title: `ReactJS para bien o para mal, es el que he elegido.`,
      text: `Mientras otros dirían porque reactJS, debes optar por GO, Angular, (Vue ya que estás relacionado con 
                php), o tal vez, Node o Python, al final creo que con los avances que ha tenido además de que la curva de aprendizaje será 
                más corta, el proceso de pasar a react native la cual es mi meta con todo el poder de  combinarlo con llamadas a apis, 
                firebase y tal, el cual está bastante completo como para 
                crear web, app modernas y demas, intentaré programar este portal sacando el máximo de potencial que obtendré al combinarlo 
                con firebase. Y que mejor muestra de migrar mi web a react para predicar con el ejemplo por lo tanto si quieres tener una 
                idea de mis conocimientos inicia con dejar tus comentarios aquí tal vez me des una idea de como mejorarlo.`,
    },
    {
      selector: `selected_webpack`,
      title: `WebPack es un conocimiento que no puede faltar`,
      text: `Usado por frameworks como React o Angular hasta en el desarrollo de aplicaciones tan conocidas como Twitter, Instagram, 
  PayPal o la versión web de Whatsapp. Conocer como usar este empaquetador de módulos es sumamente importante para estar a la par con 
  los avances actuales en desarrollo web, no tanto por estar presente en la mayoría de frameworks, sino por la potente ventaja que tiene
  el utilizarlo.<br><br>
  Minificadores de css, compiladores de sass, dependencias de desarrollo, linters, son pocas las herramientas que no pueden faltar en todo
  proyecto en la actualidad por lo tanto no conocer de webpack crea un gran vacio en los que estan desarrollando en la actualidad.`,
    },
  ],
  topdata_en: [
    {
      selector: `selected_JavaScript`,
      title: `Enought experience such as adapt myself to anywhere framework`,
      text: `have passed, more of five years, that I knew the framework jquery, but not passed so much time when recognized if I work 
  directly whit javascript, this can open  new doors at knowledges bigger like angular, reactJS, no-relationship databases, 
  such as nextJS.<br><br> At this moment, I have more of five years work with different frameworks, for this, when need a new knowledge, 
  only I read her documentation, and is easy to implement and use this new information, maybe this will not possible if I only keep whit jquery.`,
    },
    {
      selector: `selected_Maquetador`,
      title: `Speed in executing and adaptability on pixel perfect`,
      text: `Whit ultimate tendency of layout in the proper use of label in position that help at search engines to index
  web content on efficient wey, as well as correct use of last technologies of layout design such as -flexbox- and styles dedicated
  at each browser, that is the applied knowledges and oriented tp adaptability of diferent mobile devices.<br><br>
  To this, combined whit more of five year old of cotinued experience where I use techniques of planning for finaly
  customize get a great results.`,
    },
    {
      selector: `selected_phpmysql`,
      title: `Five years experience ...there is much to learn, is the right`,
      text: `I have build so much stuff in php like implement code for build theme wordpress, joomla ect... CRUD, drive knowledge for apply 
  user session including solid methodology and more, concepts MVC, function and class, PDO ect..<br><br>
  Is to my familiar use composer for install dependencies, or use php for build API rest Services, including security parameter in projects
  before haved implement library like strype and paypal, or send mail whit SPMT and more. at the end, from build a landing page or make a theme 
  for wordpress, I'm maked a lot of project use php `,
    },
    {
      selector: `selected_Wordpress`,
      title: `I'm not a plugin installer, I'm so much more than this.`,
      text: `I have seen a lot of profiles, those say, I'm wp developer, but exist several skills for say this, because is not sufficient add a menu or 
  install a theme, is necesary know about Database, Javascript, php. Is higly mantadory get knowled whit the last tendencys to create actualized web 
  site in wordpress, you haved got skil about webpack, node, AFC, make Plugin, maybe simple but know how build this ect..<br><br>
  In this aspect my profile is diferent because my experience I got was got all this knowledges to make a full project by myselft including, planing 
  time, Grapic design, frontEnd, Backend ect`,
    },
    {
      selector: `selected_adobe`,
      title: `Since 1998... All init whit photoshop.`,
      text: `photoshop, Ilustrator, corel, whit a carrer like degree in grapic design, where I got knowledge in fonts, color theory, photograpic, 
  composition elements, Digital campaigning for audiovisual media.
  while I was study, not know why I need all this knowledges and how can help me in my carrer?<br>
  
  at the end, in my first work, all this knowledge , make my profile have more than know a software, have creativity before make a design. <br>
  
  I use this creativity in all aspect of design and codig, where i can offer a product whit quality in design and good code.`,
    },
    {
      selector: `selected_react`,
      title: `ReactJS is for good or bad, the framework I was selected`,
      text: `While other say me, why go to use React instead Angular or Vue if is more familiar for you and realy, I used Angular for build the app 
  that show below in works slider. after investigating about react and react native, and how easy is learn this framework whit all your potential, 
  I decided is the way to follow like front developer.<br><br>
  apart of this, can use my knowledge in javascript and pass after build app for android, including convert to ios. 

your sistem of recurcivity, hooks, and all tools for use different database, library of npm or yarn. thought other maybe will be for me more difficult, 
prefer for this go for in continue grow join my after skills`,
    },
    {
      selector: `selected_webpack`,
      title: `webpack is a skill, can't lack`,
      text: `Used by frameworks such as React or Angular even in the development of well-known applications such as Twitter, Instagram,
     PayPal or the web version of WhatsApp. Knowing how to use this module packer is extremely important to keep up with
     current advances in web development, not so much because it is present in most frameworks, but because of the powerful advantage it has
     using it.<br><br>
     CSS minifiers, sass compilers, development dependencies, linters, there are few tools that cannot be missing at all
     project at present therefore not knowing about webpack creates a great void in those who are currently developing.`,
    },
  ],
  timeline: [
    {
      site: "2010 - 2013 (Viarte Publicidad)",
      title: `Diseñador Gráfico, Enc. de Producción de materiales impreso, Diseño UI/UX`,
      description: `Diseño de todo tipo de materiales gráficos, desde 
  la creación de una identidad visual hasta el diseño de gigantografías,
  terminación e impresión de materiales diversos, MockUp para web, 
  rediseño y mejoras visuales de páginas web, diseño de logos, diagramación ect..`,
    },
    {
      site: "2013 - 2015 (ACE Bussines Group)",
      title: `Web Designer, Web Developer, Joomla theme developer, Wordpress theme developer`,
      description: `Empresa de origen español, con officinas en Rep. Dom. en donde era encargado del 
  Diseño de las paginas web para el grupo empresaroal, implementando HTML, CSS, Jquery,
  PHP, Mysql, Javascript, Materialize.`,
    },
    {
      site: "2016 - 2018 (Grupo Pages RD, -OMD-)",
      title: `Web Designer, Web Developer, Wordpress theme developer, git`,
      description: `Un gran logro por ser una de las 3 más grandes agencias publicitarias en el país, en 
  donde como encargado del departamento de diseño web. Tareas asignadas: Diseño de Landing Page,
  web sites, woocomerce, todo implementado diseño responsive, para clientes bancarios y empresas de comercio 
  electrónico, para la misma utilizar, HTML, CSS, Jquery, PHP, Mysql, wordpress, woocomerce, ACF,
  Boostrap, etc.`,
    },
    {
      site: "2018 - 2019 (Bits&BitesMedia RD)",
      title: `Web Developer, Wordpress theme developer`,
      description: `Develop RNT.DE Empresa Alemana con oficinas en REP. DOM. Como web developer 
  diseñaba web sites en wordpress, WPA, Landing Page, siendo el mayor logro la creación del portal de RNT.DE, 
  codificada a medida tomando en cuenta la compartibilidad de navegadores, diseño responsive, módulos creados con 
  javascript, y customización de carga.`,
    },
    {
      site: "2019 - 2020 (NOZAMA)",
      title: `Web Developer, Liquid, Shopify, ReactJS, Boostrap, Javascript, Node, git`,
      description: `Diseño para Shopify, implementado javascript, desarrollo de landing page, maquetado y 
  desarrollo de modulos con ReactJS, mejoras en la funcionalidad de la web, integración de liquid, 
  conección con base de datos y consumo de API de tercer.`,
    },
    {
      site: "2020 (Full of Tradition)",
      title: `Desarrollo de base de datos, PHP, Javascript, pasarela de pago, Javascript, control de versiones`,
      description: `Desarrollo personalizado y customizado de aplicación web, para la captación de talentos,
  seniors implementando un sistema de captación de información a travez de formularios, además de la 
  implementación de pasarela de pagos PayPal y Stripe en sus últimas versiones.`,
    },
    {
      site: "2021 - 2022 (PayRetailers)",
      title: `Node, WebPack, PHP, Javascript, CRM, Boostrap, ACF, Responsive Design`,
      description: `Desarrollo customizado, con inovativas herramientas, implementar nuevos requerimientos a 
      medida utilizando el modelo pixel perfect, captación de datos a través de formularios el cual alimenta 
      con datos el CRM, actividades de manteniento y mejoras continuas, control de versión con BitBucket y git.`,
    },
  ],
  timeline_en: [
    {
      site: "2010 - 2012 (Viarte Publicidad)",
      title: `Graphic designer in charge to production print material and UX/UI  Design `,
      description: `Graphic designer in charge to production print material and UX/UI  Design of all kind of print 
      material, from visual identity to giant graphic design, Mockup for web, re-design and graphic enhancement for
      websites, logo design and diagramer magazine  ect..`,
    },
    {
      site: "2012 - 2015 (ACE Bussines Group)",
      title: `Web Designer, Web Developer, Joomla theme developer, Wordpress theme developer`,
      description: `Company from spain origin, with offices in Dom Rep, where I was in charge to 
      develop all webpages for company group implementing  HTML, CSS, Jquery,
      PHP, Mysql`,
    },
    {
      site: "2015 - 2018 (Grupo Pages RD, -OMD-)",
      title: `Web Designer, Web Developer, Wordpress theme developer`,
      description: `A biggest goal on my career was being admitted for one of the 3 biggest tech agency 
      where I has in charge of the webpage developer department, Landign Pages, WooCommerce, all implemented 
      responsive design, for banking commercial, business of sales online, in the same implement, HTML, CSS, 
      Jquery, PHP, Mysql, wordpress, woocomerce, ACF, Boostrap, etc.`,
    },
    {
      site: "2018 - 2019 (Bits&BitesMedia RD)",
      title: `Web Designer, Web Developer, Wordpress theme developer`,
      description: `Company from Germany with offices in Dom Rep. where I join to developer department specific 
      like wordpress and php developer, here the biggest goal was developed the website for RNT.DE taking care 
      with compartibility with all browser and specially in responsive design, injecting inside javascript and 
      React modules and customizing charge of resources.`,
    },
    {
      site: "2019 - 2020 (NOZAMA)",
      title: `Web Developer, Liquid, Shopify, ReactJS, Boostrap, Javascript, Node`,
      description: `Develop for Shopify, design Landing page, made react modules, enhancements on web functionality, 
      connect external API and databases ect..`,
    },
    {
      site: "2020 (Full of Tradition)",
      title: `Desarrollo de base de datos, PHP, Javascript, pasarela de pago, Javascript`,
      description: `Design MySQL Database, coding with PHP, JavaScript, implement payment gateway, seniors talent acquisition 
      system thought forms including payment with PayPal and Stripe in your last version.`,
    },
    {
      site: "2021 - 2022 (PayRetailers)",
      title: `Node, WebPack, PHP, Javascript, CRM, Boostrap, ACF, Responsive Design`,
      description: `Custom development of web enhancements, catch data through forms system connected to CRM for record this data, 
      continuous enhancements and version control with git and Bitbucket`,
    },
  ],
};
