import Image from 'next/image'
import Link from 'next/link'
import { useState } from 'react'
import { useRouter } from 'next/router'
import Head from 'next/head'
import Analytics from 'analytics'
import googleTagManager from '@analytics/google-tag-manager'




let blockScroll = () => {
  window.onscroll = () => {
    var y = window.scrollY
  }
}

const analytics = Analytics({
  app: 'awesome-app',
  plugins: [
    googleTagManager({
      containerId: 'AW-300373562'
    })
  ]
})


analytics.page()


analytics.on('event', 'conversion', { 'send_to': 'AW-300373562/EDBdCK_GlvQCELqsnY8B' })

let Header = () => {
  const router = useRouter()

  const [actLeng, setActLeng] = useState(() => {
    let ActFlag = router.locale == "es" ? "/es.svg" : "/en.svg";
    return ActFlag
  })
  console.log('actLeng', actLeng)

  if (router.events) {
    router.events.on('routeChangeComplete', (url, { shallow }) => {
      if (router.locale == "es") {
        setActLeng("/es.svg")
      } else {
        setActLeng("/en.svg")
      }

    })
  }




  return (
    <>
      <script
        async
        src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
      />
      <script
        dangerouslySetInnerHTML={{
          __html: `
               (adsbygoogle = window.adsbygoogle || []).push({
                   google_ad_client: "AW-300373562",
                   enable_page_level_ads: true
              });
                `
        }}
      />
      <header className="header">
        <div className="header__wrapper">
          <div className="header__wrapper-logo">
            <Link href="/"><a>
              <Image className="header__img" src="/logo.png" layout="fixed" alt="ricardo lavour" width="50" height="50" />
            </a>
            </Link>
          </div>

          <div className="header__wrapper-right-menu">
            <ul className="header__wrapper-right-menu-ul">
              <li className="header__wrapper-right-menu-ul-blog">
                <Link href="/blog" passHref>
                  <a>
                    blog
                  </a>
                </Link>
              </li>
        

                
                  <li className="header__wrapper-right-menu-ul-menu">
                    <span><Image src={actLeng} width="15" height="15" layout="fixed" alt="selected languaje" /></span>
                    <a><Image src="/animated.gif" width="35" height="35" layout="fixed" alt="select languaje" /></a>
                      <ul>
                          <li>
                            <Link href={router.pathname} locale="es" scroll={false}>
                              <a><Image src="/es.svg" width="25px" height="20px" layout="fixed" alt="select languaje es" /></a>
                            </Link>
                          </li>
                          <li>
                            <Link href={router.pathname} locale="en" scroll={false}>
                              <a><Image src="/en.svg" width="25" height="20" layout="fixed" alt="select languaje en" /></a>
                            </Link>
                          </li>
                        </ul>
                    </li>
                
            </ul>

            <div className="burger-wrapper">
              <span onMouseEnter={blockScroll} className="triguer_mn"></span>
              <li className="burger-wrapper__line-top"></li>
              <li className="burger-wrapper__line-center"></li>
              <li className="burger-wrapper__line-bottom"></li>

              <nav className="header__bigpanel">
                <div className="bigpanel_wrapper">
                  <ul className="bigpanel_wrapper__menuLeft">
                    <li><a>Javascript</a></li>
                    <li><a>Base de datos</a></li>
                    <li><a>php</a></li>
                    <li><a>Maquetación</a></li>
                    <li><a>Gitlab</a></li>
                    <li><a>WebPack</a></li>
                  </ul>
                  <ul className="bigpanel_wrapper__menuRight">
                    <li><a>Diseño Gráfico</a></li>
                    <li><a>Producción</a></li>
                    <li><a>Diagramación</a></li>
                  </ul>
                </div>
              </nav>

            </div>

          </div>
        </div>
      </header>
    </>
  )
}
export default Header