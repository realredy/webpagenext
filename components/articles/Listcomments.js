import {useState,useEffect} from 'react';
import firebase from 'firebase'; 
import {db} from '../../firebase/firebase';
import 'firebase/firestore';
 
let Listcomments = ({id})=>{

const [comments, setComments] = useState([])
 
   useEffect(()=>{
    async function getComments (){
        let comment = await firebase.firestore(db).collection('comments').doc(id)
        .collection('allcommets').where("aproved", "==", true).get();  
       let hasCommets =  comment.docs.length != 0 ?   hassCommet(comment.docs) : setComments(null) ;
      } 
      function hassCommet(dataCommets){ 
          let allCommets = [];
        dataCommets.map((e)=>{ 
          allCommets.push(e.data())
        }) 
        
        setComments(allCommets);
      }

    getComments()
},[])


    if(comments == null){
        return(
            <>
             <span>No hay comentarios...</span>
            </>
        )
    }else{
    return(
        <>
        <div className="ModuleComments">
            <span className="ModuleComments_head">Lista de comentarios </span>
            <ol>
            {
            comments.map((coment, i)=>{
                 const commentDate = new Date(coment.date.seconds*1000)
              return(
              <li key={i}>
                    <div className="ModuleComments_list">
                      <span>De: {coment.name}</span> 
                      <pre>En: {`${commentDate.getDate()}-${commentDate.getMonth()}-${commentDate.getFullYear()}`}</pre>
                    </div>
                    <div className="ModuleComments_comment">
                        <p>{coment.message}</p>
                      </div>
                </li>
              )
              })
            }  
            </ol>
        </div>
       
        </>
    )
    }
}
 
export default Listcomments