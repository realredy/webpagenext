import firebase from 'firebase'; 
import {db} from '../../firebase/firebase';
import 'firebase/firestore';

  
let SistemComments =({id})=>{ 
     
let quizPased = false;
    
    /*==========================================================*/
    /*====================   SOLVE QUIZ ========================*/
    /*==========================================================*/
    function generateRandom(max){
        return Math.random() * max;
    }
    
    function solvequiz(e){
         const face = document.getElementById("face")
         const quiz = document.getElementById(e.target.id)
         const valor1 = document.getElementsByClassName('comment-control-dato1')[0].innerText;
         const valor2 = document.getElementsByClassName('comment-control-dato2')[0].innerText;
         const result = parseInt(valor1) + parseInt(valor2)
        if( parseInt(e.target.value) === result){ 
            quiz.style.border = 'solid 2px green';
            face.innerHTML = '👍‍‍'; 
            quizPased = true;
        }else{
            quiz.style.border = 'solid 2px red';
            face.innerHTML = '👎‍'; 
            quizPased = false;
        }
       }

     /*==========================================================*/
     /*====   FILTRO PARA ANTI JACK EN EL INPUT NOMBRE  =======*/
     /*==========================================================*/
     let filterNme = (e)=>{
      
        let inpt = document.getElementById(e.target.id);
        let str = inpt.value;
      
           let re = /[<>\[\]=+/*%!#$`|\\^&;:(")'}{-]/g;
           let result = re.test(str);
           if(result == true){
              inpt.checkValidity();
              inpt.classList.add('badCharter');
              let reparatext = str.replace(re, '');
              setTimeout(()=>{
                 inpt.value = reparatext;
                 inpt.checkValidity();
                 inpt.classList.remove('badCharter');
              },1000);
              
           } else {  
              inpt.classList.remove('badCharter');
           }
  
      }

     /*==========================================================*/
     /*====      VALIDATE FORM BEFORE UNBLOCK BUTTON      =======*/
     /*==========================================================*/

   let validateForm = (e)=>{
      e.preventDefault();
       let list = ['name','email','commet'];
          let itemsForm = e.target.parentNode;//array all item of form
          let elementOfForm = [];
           for (let i = 0; i < 4; i++) {
               if(itemsForm[i].name === list[i]){
                   if(!itemsForm[i].value){ 
                        itemsForm[i].style.border = "solid 3px red"; 
                   }else{ 
                    itemsForm[i].style.border = "initial";
                    elementOfForm.push(itemsForm[i].value) 
                   } 
               }
           }

           let re = /[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]/g;
           let result = re.test(elementOfForm[1]);
            
               if(!result){
                itemsForm[1].style.border = "solid 3px red"; 
                return;
               }else{
                if(elementOfForm.length === 3 && quizPased){
                   recordCometsInDatabase(elementOfForm, itemsForm)
                }
          } 
        
   }


     /*==========================================================*/
     /*====      RECORD THIS COMENT IN DATABASE FRB      =======*/
     /*==========================================================*/

   let recordCometsInDatabase = async (items, form) => {
     const message = document.getElementsByClassName('nocommets-form-message')[0]
       console.log(items)
        let data = {
            aproved: false,
            date: firebase.firestore.Timestamp.fromDate(new Date()),
            name:    items[0],
            email:   items[1],
            message: items[2]
        }

       try{   
        const dbr = firebase.firestore(db);
        let create_article = dbr.collection('comments').doc(id).collection('allcommets').doc(); 
         await create_article.set(data,{merge:true}).then((e)=>{ 
                form.reset();
                message.style.backgroundColor = 'green';
                message.style.color = 'black';
                message.innerText = 'Soon we can aproved your message, thanks for commet';
            setTimeout(()=>{
                message.style.backgroundColor = 'black';
                message.style.color = 'black';
                message.innerText = ' ';
            },7000) 
        }) 
           
            
    
       } catch (error){
        console.log("Resultados de un error al guardar: ",error) 
        message.style.backgroundColor = 'red';
        message.style.color = 'black';
        message.innerText = error;
       } 

   }

   
        return(
            <>
                <div className="nocommets">
                    <div className="nocommets-head">
                        
                    </div>
                    <div className="nocommets-form">
                        <div className="nocommets-form-head">
                            <span>Inserte su comentario..  </span>
                        </div>
                        <div className="nocommets-form-submitDate">
                            Los datos subministrados aqui no serán compartidos o divuldados para uso de terceros, su email
                            solo será usado para informarle que su comentario ha sido publicado, nombre y comentario serán 
                            visible para el público que entre a esta web, en caso de ser aprobado. Si luego de emitir un comentario
                            desea que no lo publiquemos, puedes escribirnos via email para su evaluación.
                        </div>
                        <form action="">
                            <label htmlFor="commentName">Nombre</label>
                            <input onChange={filterNme} type="text" name="name" id="commentName" required/>
                            <label htmlFor="commentEmail">Email</label>
                            <input onChange={filterNme} type="email" name="email" id="commentEmail" required />
                            <label htmlFor="commet">Comentario</label>
                            <textarea onChange={filterNme} name="commet" id="commet" cols="30" rows="10" required></textarea>
                            <div className="comment-control">
                                <code>Resuelva el quiz y podrá comentar</code>
                            <span className="comment-control-dato1">{Math.floor(generateRandom(9))}</span>+
                            <span className="comment-control-dato2">{Math.floor(generateRandom(9))}</span>
                            =<input  className="comment-control-quiz" onChange={solvequiz} type="text" id="quiz" />
                            <span id="face"></span>
                            </div>
                            <input onClick={validateForm} type="submit" value="enviar comentario"  />
                        </form>
                        <span className="nocommets-form-message"></span>
                    </div>
                </div> 
            </>
        )
    
  
}
export default SistemComments;