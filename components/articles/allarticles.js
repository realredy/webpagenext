import {useState,useEffect} from 'react'
import firebase from 'firebase'; 
import {db} from '../../firebase/firebase';
import 'firebase/firestore';
import Image from 'next/image';
import Editarticle from './editArticle'; 


export default function Allarticles({categories}){
    const [GetArticles, setGetArticles] = useState()
    const [Ids, setIds] = useState()
    const [Openeditor, setOpeneditor] = useState()
      const [ImagesAll, setImagesAll] = useState()
      

    let edditfunction = (id)=>{
      
        setOpeneditor(<Editarticle cat={categories} id={id.target.id} img={ImagesAll}/>)
    }
    let closepanel = ()=>{
        setOpeneditor(null)
    }


    useEffect(() => {
        // ========= TRY TO FECH ALL IMAGES ======== //
        let FechImages = async ()=>{

        
       let images = []
       let alts = []
       await firebase.storage(db).ref("all/").listAll()   
              .then((imagesComming)=>{
                  imagesComming.items.map( (filterImages)=>{ 
                      filterImages.getDownloadURL().then((imageResult)=>{   
                               images.push(imageResult)  
                               
                        })  
                      //=======  
                      filterImages.getMetadata().then((metadata)=>{
                        alts.push(metadata.customMetadata.alt)  
                        // console.log('metadata<><><><>>',metadata.customMetadata.alt)
                      })
                  })  
              }) 

              setImagesAll({img:images,alt:alts})

            }
            FechImages()  
      // ========= TRY TO FECH ALL IMAGES ======== //
    }, [ ])
 
useEffect(() => {
    
  let FetTotaldata = async ()=>{ 
    let articlecollection = []  
    let mixid = []
    let getDataFromFirebase = await firebase.firestore(db).collection('articles').get(); 
         let dataCatchFromFirebase  =  getDataFromFirebase.docs; 
           dataCatchFromFirebase.map((transferDatatoProps)=>{   
                      articlecollection.push(transferDatatoProps.data())  
               }) 
               
               dataCatchFromFirebase.map((transferDatatoProps)=>{ 
                mixid.push(transferDatatoProps.id)   
               }) 
 
      setIds(mixid)          
     setGetArticles(articlecollection)  
   }

FetTotaldata(); 

},[])



//  console.log(da)

if(!GetArticles){
    return(
        <>
         <div className="allarticles">
        <pre>Wait we are chargin data....</pre>
        </div>
        </>
    )
}else{


    return(
        <> 
        <div className="allarticles">
        {Openeditor}
            <span>Todos los articulos!</span>
            <span id="closePanel" onClick={closepanel}></span>
            <table width="100%" cellPadding="0" cellSpacing="0">
                <thead>
                     <tr>
                      <td width="13%"><b>Image</b></td>   
                      <td width="70%"><b>Title</b></td>  
                      <td width="17%" colSpan="2"><b>Action</b></td>  
                       </tr>
                </thead>
                <tbody>
                     
                    
            { GetArticles.map((artc,y)=>{
                return(
                    <tr key={y}>
                    <td align="center"><Image src={artc.image} width={90} height={35} alt="image article"/></td>   
                    <td>{artc.friendlyUrl.replaceAll('-',' ').toUpperCase()}</td>  
                    <td><button id={Ids[y]} onClick={edditfunction}  className="allarticles__button_eddit">eddit</button></td>  
                    <td><button className="allarticles__button_delette">delette</button></td>  
                     </tr>
                )
            })}
            
                </tbody>
            </table>
        </div>
        
        </>
    )
}
} 