import {useState,useEffect} from 'react'
import firebase from 'firebase'; 
import {db} from '../../firebase/firebase';
import 'firebase/firestore'; 
import dynamic from "next/dynamic";
import 'suneditor/dist/css/suneditor.min.css'; 
import Image from 'next/image';
const SunEditor = dynamic(() => import("suneditor-react"), {
    ssr: false,
  });





let Editarticle = ({cat,id,img})=>{

const [article, setArticle] = useState()
const [dataForm, setDataForm] = useState([{title:'',
                                            friendlyUrl:'',
                                            html:'',
                                            image:'',
                                            imageAlt:'',
                                           }])
const [Allimages, setAllimages] = useState()

let closeEditor = () =>{
    let experimetn = document.getElementById('closePanel')
    experimetn.click()
}

// this data comming in string for props
const transformCat = JSON.parse(cat)
// getting only the values of this object 
const catToArray = Object.values(transformCat)
 

let closePannelImages = (e)=>{
    e.preventDefault()
    let pannelImagesContainner = document.getElementsByClassName('pannelImagesContainner')[0]
    let isClosed = pannelImagesContainner.classList.contains('closePannel')
    switch (e.target.id) {
        case 'pannelImagesClose':
            !isClosed ? pannelImagesContainner.classList.add('closePannel') : null;
            break;
            case 'pannelImagesOpen':
                isClosed ? pannelImagesContainner.classList.remove('closePannel') : null;
                break;
        default:
             pannelImagesContainner.classList.add('closePannel') 
            break;
    } 
}
      
let itemsTosave = (e)=>{
    e.preventDefault();
// this function to set to usestate all items
// only need get the name of the item
  console.log('date of form  ::',e.target.name)
 
}


let setImageToForm = (e)=>{
    console.log(e)
    let bucket_eddit_image = document.getElementById('bucket_eddit_image')
    bucket_eddit_image.value = `${e.target.id}|${e.target.alt}`; 
}

 //--------------------------------------------------------------------//

        


function handleChange(content){ 
    let bucketHTML = document.getElementById('herencHTML');
    bucketHTML.value = content;
  }

let saveDataToFirebase = async (e)=>{
  e.preventDefault();
let elemens = ['article_title','article_url','category','html','eddit_image']
 var formData = [];


 

for (let i = 0; i < elemens.length; i++) { 
     formData.push(document.getElementsByName(elemens[i])[0].value);
    // formData.push(document.getElementsByName(elemens[i])[0]);
} 
 

 


let body = {
    title:formData[0],
    friendlyUrl:formData[1].replaceAll(' ','-'),
    category:formData[2],
    html: formData[3] != ''? formData[3] : article.html,
    image:formData[4] != ''? formData[4].split('|')[0]:article.image,
    imageAlt:formData[4] != ''? formData[4].split('|')[1]:article.imageAlt,
} 
console.log("🚀 ~ file: editArticle.js ~ line 105 ~ saveDataToFirebase ~ body", body)

 

try{   
    const db = firebase.firestore(db);
    let create_article = db.collection('articles').doc(id); 
    const env = await create_article.set(body,{merge:true}).then((e)=>{
    //    spinner.style.opacity = 0;
    //    form.reset();
    //    preview.style.backgroundImage = 'unset';
    location.reload();
    }) 

} catch (error){
    console.log("Resultados de un error al guardar: ",error) 
} 


}



useEffect(() => {
    let filterImages = (imagesPlusAlt)=>{
        let arrayOfImages = []
             if(imagesPlusAlt){ 
              for (let i = 0; i < imagesPlusAlt.img.length; i++) {
                arrayOfImages.push(`${imagesPlusAlt.img[i]}|${imagesPlusAlt.alt[i]}`) 
               } 
            } 
            setAllimages(arrayOfImages)
    } 
  filterImages(img) 
},[])  


useEffect(() => {
   async  function getarticle(idArticle){
    let getDataFromFirebase = await firebase.firestore(db).collection('articles').doc(idArticle).get(); 
    let dataCatchFromFirebase  =  getDataFromFirebase.data();  
       
          setArticle(dataCatchFromFirebase)          
     }
     getarticle(id) // this id coming from props  
},[])

// console.log(article)


 

if(!article){
    return(
        <>
         <div className="allarticles">
        <pre>Wait we are chargin data....</pre>
        </div>
        </>
    )
}else{ 
return(
    <> 
     <div className="eddit">
         <div className="eddit__wrapper">
             <div className="eddit__wrapper-closeButton">
                 <button onClick={closeEditor}>close x</button>
             </div>
             <div className="eddit__wrapper-title">
                 <span>Actualy editing article id: <b>{id}</b></span>
             </div>

      <div className="eddit__wrapper-form"> 
                        <form onSubmit={saveDataToFirebase} className="eddit__wrapper-form_form">
                            <div className="eddit__wrapper-form_form-left"> 
                        <label htmlFor="label-title">insert title for this article</label>
                        <input onChange={itemsTosave} type="text" name="article_title" id="label-title" defaultValue={article.title} />
                        <label htmlFor="label-url">Insert fryendly url</label>
                        <input onChange={itemsTosave} type="text" name="article_url" id="label-url" defaultValue={article.friendlyUrl.replaceAll('-',' ')} />
                        <label htmlFor="SelectCategory">Select Category</label>
                        <select onChange={itemsTosave} name="category" id="SelectCategory" className="eddit__wrapper-form_form-left-select">
                            <option defaultValue={article.category}>{article.category}</option>
                            {
                                catToArray.map((itemsCat,e)=>{ 
                                    return(
                                    <option key={e} value={itemsCat}>{itemsCat}</option>
                                    )
                                })
                            }
                        </select>
                        <input type="hidden" name="html" id="herencHTML" />
                        <SunEditor  onChange={handleChange}  
                                 defaultValue={article.html}
                                 setOptions={{
                                 height:200,
                                 buttonList: [['undo', 'redo'],['formatBlock'],
                                 ['font','fontSize', 'align','list','bold', 'underline','hiliteColor','fontColor'], 
                                 ['table', 'link','image','codeView']]
                            }}/>
                            <input className="eddit__wrapper-form_form-left-submit" type="submit" value="save article" />
                            </div>
                            <div className="eddit__wrapper-form_form-right"> 
                               <div className="eddit__wrapper-form_form-right-wrapper">
                                   <div style={{ backgroundImage:`url(${article.image})`,backgroundSize: 'cover' }} className="eddit__wrapper-form_form-right-wrapper_preview">

                                   </div>
                                   <input onChange={itemsTosave} type="hidden" name="eddit_image" id="bucket_eddit_image" />
                                   <button id="pannelImagesOpen" onClick={closePannelImages}>change image</button>
                                        
                                       <div className="eddit__wrapper-form_form-right-wrapper-imagesList">
                                           
                                           <ul className="pannelImagesContainner closePannel"> 
                                           <button id="pannelImagesClose" onClick={closePannelImages}>↑ Close pannel images ↑</button>
                                       { 
                                        Allimages.map((images, e)=>{  
                                            let imagesSplit = images.split('|');
                                                return (
                                                    <li  key={e}>
                                                        <Image onClick={setImageToForm} id={imagesSplit[0]}
                                                        src={imagesSplit[0]} alt={imagesSplit[1]} 
                                                        width={300} height={120} />
                                                    </li>
                                                ) 
                                            }) //images
                                        } 
                                         
                                    </ul>
                                </div> 
                               </div>
                            </div>
                        </form> 
      </div>
         </div>
     </div>
    </>
)
            }
}
export default Editarticle;