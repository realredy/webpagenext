import { useRouter } from 'next/router'

function Printsoftware() {
    const router = useRouter() 
    return (
        <>
        <div id="prf" className="boxed">
        <div className="wrapperPrf">
            <div className="prf_left">
                <div className="prf_aros">
                    <span> 
                        {router.locale === 'en' ? `6+ experience on Graphic Design` : `6+ Años
                        experiencia
                        diseño gráfico`}
                       </span>
                    <div className="prf_red">
                        <div className="prf_grisSuave">
                            <div className="prf_gris">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div className="prf_descripcionAros">
                    <ul>
                        <li>{router.locale === 'en' ? `Design` : `Diseño`}</li>
                        <li>{router.locale === 'en' ? `Producction` : `Producción`}</li>
                        <li>{router.locale === 'en' ? `Diagramming` : `Diagramación`}</li>
                    </ul>
                </div>
            </div>
                    
            <div className="prf_right">
                    <div className="wrapper_right">
                    <ul id="prf_nombres">
                        <li>Swift 3D</li>
                        <li>Freehand</li>
                        <li>Corel Draw</li>
                        <li>After Efects</li>
                        <li>indesign</li>
                        <li>Ilustrator</li>
                        <li>Fotoshop</li>
                        <li></li>
                    </ul> 
                    <ul id="prf_nivel">
                        <li><span className="barraSwift"></span> </li> 
                        <li><span className="barrafreehand"></span></li>
                        <li><span className="barraCorel"></span></li>
                        <li><span className="barraEfects"></span></li>
                        <li><span className="barraIndesign"></span></li>
                        <li><span className="barraIlustrator"></span></li>
                        <li><span className="barraFotoshop"></span></li>
                        <li id="prf_empty"></li>
                        
                    </ul>

                    </div>
                        <ul id="anosPrf">
                            <li>{router.locale === 'en' ? `1 Year` : `1 Año`}</li>
                            <li>{router.locale === 'en' ? `2 Year` : `2 Año`}</li>
                            <li>{router.locale === 'en' ? `3 Year` : `3 Año`}</li>
                            <li>{router.locale === 'en' ? `4 Year` : `4 Año`}</li>
                            <li>{router.locale === 'en' ? `5 Year` : `5 Año`}</li>
                            <li>{router.locale === 'en' ? `6 Year` : `6 Año`}</li>
                        </ul>
                </div>
        </div>
        </div>
        </>
    )
}

export default Printsoftware
