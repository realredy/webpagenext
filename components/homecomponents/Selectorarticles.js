import Image from 'next/image';
import { fulldata } from '../../aplicJson/index';
import { useRouter } from 'next/router';
import {useState, useEffect} from 'react';


   
function Selectorarticles(props) {
const router = useRouter();
 

const [innerData, setInnerData] = useState(()=>{
   let leng =  router.pathname === 'en' ? fulldata.topdata_en : fulldata.topdata ; 
   return leng;
})

const [lang, setLang] = useState()



 
 
 router.events.on('routeChangeComplete', changeDatal)
 
  

function changeDatal(url, { shallow }){  
// console.log("🚀 ~ file: Selectorarticles.js ~ line 33 ~ changeDatal ~ url", url)

        let getdata = url === "/" ? fulldata.topdata_en : fulldata.topdata ;

        let selected = document.querySelector('.listaTag .selectedHome');
        let titulo = document.getElementById('titleHomeBox');
        let texto = document.getElementById('textHomeBox');

        if(selected != null){
            let textChange =  getdata.filter((array)=>array.selector == selected.classList[0])
            //   console.log("🚀 ~ file: Selectorarticles.js ~ line 56 ~ changeData ~ selected", selected)  
            titulo.innerHTML = textChange[0].title;
            texto.innerHTML = textChange[0].text; 
        } 

        setLang(url)
        setInnerData(getdata)
}


 
function changeData(e){ 
   
    // console.log('router.pathname===>', router)

        let actual = document.querySelector('.listaTag .selectedHome');
        actual.classList.remove('selectedHome');
        let addnew = document.getElementsByClassName(e.target.classList[0]);
        addnew[0].classList.add("selectedHome");

        let titulo = document.getElementById('titleHomeBox');
        let texto = document.getElementById('textHomeBox');

        const button = e.target.classList[0]
         
       let switchData = [];

        // if(!lang){
            let setPermData = router.locale === "en" ? fulldata.topdata_en : fulldata.topdata ;
            let selecteds =  setPermData.filter((array)=>array.selector == button) 
            switchData = selecteds;
        // }else{
        //     let selected =  innerData.filter((array)=>array.selector == button) 
        //     switchData = selected;
        // }
 
        titulo.innerHTML = switchData[0].title;
        texto.innerHTML = switchData[0].text; 
   }






    return (
        <>
           <div id="home_body">
              <div className="leftImage">
                  <Image src='/robot_final.png'  alt="ricardo enrique perez lavour" width={250} height={372} />

                  </div>

                 <div className="right_TabsText">
                    <div className="boxTags"> 
                        <ul className="listaTag">
                            <li onClick={changeData} className="selected_Maquetador selectedHome">Maquetador</li>
                            <li onClick={changeData} className="selected_JavaScript">JavaScript</li>
                            <li onClick={changeData} className="selected_phpmysql">PhP y MySQL</li>
                            <li onClick={changeData} className="selected_Wordpress">Wordpress</li>
                            <li onClick={changeData} className="selected_adobe">Pack Adobe</li>
                            <li onClick={changeData} className="selected_react">ReactJS</li> 
                            <li onClick={changeData} className="selected_webpack">webpack</li> 
                        </ul>
                    </div>
                    <div className="textBox"> 
                        {/* <div id="hideScroll"></div> */}
                        <div id="innerTextBox">
                        <h1 id="titleHomeBox">{router.locale === 'en' ? `${fulldata.topdata_en[1].title}`:
                        `${fulldata.topdata[1].title}` } 
                        </h1>
                            <span id="textHomeBox">{router.locale === 'en' ? `${fulldata.topdata_en[1].text}`:
                        `${fulldata.topdata[1].text}` } </span>
                        </div>
                    </div>

                 </div>
            </div>                    
        </>
    );
}



export default Selectorarticles;