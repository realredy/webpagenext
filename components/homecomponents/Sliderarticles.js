import {fulldata} from '../../aplicJson/index';
import { useRouter } from 'next/router';
import { CarouselProvider, 
         Slider, 
         Slide,   
         ButtonBack,
         ButtonNext } from 'pure-react-carousel';

let Sliderarticles = ()=>{
  
  const router = useRouter();
  let dataSlider =  router.locale === 'en' ? fulldata.sliders_en : fulldata.sliders ; 
  

 return(
   <div className="slid">
   <section className="slid__Wrapper">
     <CarouselProvider
        naturalSlideWidth={100}
        naturalSlideHeight={155}
        totalSlides={7}
        infinite={true}
        isIntrinsicHeight={true}
      > 
      <Slider>
       {
          dataSlider.map((output)=>{ 
            return(
             <Slide key={output.id} index={output.id}>
              <div className="wrapperSld">
                <h2>{output.title}</h2>
                <span>{output.subtitle}</span>
                <p>{output.text}</p>
              </div>
          </Slide> 
                )
        })
       }
          
       </Slider>
       <div className="wrapper_brn_sld">
        <ButtonBack>{router.locale === 'en' ? 'Back':'atrás'}</ButtonBack>
        <ButtonNext>{router.locale === 'en' ? 'Next':'siguiente'}</ButtonNext>
        </div>
     </CarouselProvider>
     </section>
     </div>
)
}

export default Sliderarticles 