import React, { useState } from "react";
import { fulldata } from "../../aplicJson/index";
import { useRouter } from "next/router";
import {
  CarouselProvider,
  Slider,
  Slide,
  ButtonBack,
  ButtonNext,
} from "pure-react-carousel";

// import {useState} from 'react'

function Timeline() {
  const router = useRouter();
  const [windowWidth, setWindowWidth] = useState(2);

  window.addEventListener("resize", () => {
    let cuantityOfSlides = window.innerWidth >= 992 ? 2 : 1;
    setWindowWidth(cuantityOfSlides);
  });

  let getTimeline =
    router.locale === "en" ? fulldata?.timeline_en : fulldata?.timeline;

  return (
    <>
      <div className="timeline">
        <div className="timeline__wrapper">
          <CarouselProvider
            visibleSlides={windowWidth}
            totalSlides={7}
            infinite={false}
            isIntrinsicHeight={true}
          >
            <div className="timeline__wrapper-btnBack">
              <ButtonBack></ButtonBack>
            </div>

            <Slider>
              {getTimeline &&
                getTimeline.map((timeline, index) => {
                  const isOdd = index % 2 === 0;
                  return (
                    <>
                      <Slide key={index} index={index}>
                        <div className="timeline__wrapper_slides">
                          <div
                            className={`${"timeline__wrapper_slides_content"}${
                              isOdd ? " odd" : ""
                            }`}
                          >
                            <div className="timeline__wrapper_slides_content_top">
                              <div className="timeline__wrapper_slides_content_top-wrapper">
                                <h4>{timeline.title}</h4>
                                <p>{timeline.description}</p>
                              </div>
                            </div>
                            <div className="timeline__wrapper_slides_content_bottom">
                              <div className="timeline__wrapper_slides_content_bottom-wrapper">
                                <span className="timeline__wrapper_slides_content_bottom-wrapper-span">
                                  {timeline.site}
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </Slide>
                    </>
                  );
                })}
            </Slider>
            <div className="timeline__wrapper-btnNext">
              <ButtonNext></ButtonNext>
            </div>
          </CarouselProvider>
        </div>
      </div>
    </>
  );
}

export default Timeline;
