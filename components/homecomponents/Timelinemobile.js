import React, { useState } from "react";
import { fulldata } from "../../aplicJson/index";
import { useRouter } from "next/router";

const TimelineMobile = () => {
  const router = useRouter();
  //   const [selectLanguaje, setSelectLanguaje] = useState(() => {
  //     return router.locale === "en" ? fulldata?.timeline_en : fulldata?.timeline;
  //   });

  let selectLanguaje =
    router.locale === "en" ? fulldata?.timeline_en : fulldata?.timeline;

  return (
    <>
      <div id="ContainerTimelineMobile">
        {selectLanguaje.map((mobilTimeline, index) => {
          const getYear = mobilTimeline.site.split("(")[0].trim();
          const getNameCompany = "(" + mobilTimeline.site.split("(")[1];
          console.log(
            "🚀 ~ file: Timelinemobile.js ~ line 19 ~ {selectLanguaje.map ~ mobilTimeline.site.s [0]",
            mobilTimeline.site.split("(")[1]
          );

          return (
            <ul key={index}>
              <li>
                <h3 className="red">{getYear}</h3>
              </li>
              <li>
                <span> {`${mobilTimeline.title} ${getNameCompany}`} </span>
                <p> {mobilTimeline.description} </p>
              </li>
            </ul>
          );
        })}

        {/* <ul>
                   <li><h3 className="red">2015 - 2017</h3></li>
                   <li>
                   <span>
                                {router.locale === 'en' ? fulldata.timeline_en[0].title : fulldata.timeline[0].title }
                            </span>
                          <p> 
                                {router.locale === 'en' ? fulldata.timeline_en[0].description : fulldata.timeline[0].description }
                            </p>
                            </li>
               </ul>

 <ul>
                   <li><h3 className="red">2017 - 2018 </h3></li>
                   <li>
                   <span>
                                {router.locale === 'en' ? fulldata.timeline_en[1].title : fulldata.timeline[1].title }
                            </span>
                          <p> 
                                {router.locale === 'en' ? fulldata.timeline_en[1].description : fulldata.timeline[1].description }
                            </p>
                            </li>
               </ul>

               
               <ul>
                   <li><h3 className="red">2019 - 2020 </h3></li>
                   <li>
                   <span>
                                {router.locale === 'en' ? fulldata.timeline_en[2].title : fulldata.timeline[2].title }
                            </span>
                          <p> 
                                {router.locale === 'en' ? fulldata.timeline_en[2].description : fulldata.timeline[2].description }
                            </p>
                            </li>
               </ul> */}
      </div>
    </>
  );
};

export default TimelineMobile;
