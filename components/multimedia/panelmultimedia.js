 import Image from 'next/image'
 import { useState } from 'react'

 let simpleClossePannel = (e)=>{
  document.getElementsByClassName('panelmultimedia')[0].style.display = 'none' 
}

let Panelmultimedia = ({allImages}) => {
  const [langSwitch, setLangSwitch] = useState('switch_firebase')
  
  
   
let Switchlang = ( e )=>{
      console.log(e.target.classList[1])
      setLangSwitch(e.target.classList[1])
}
 

  let setImageToForm = (e)=>{ 
     if(document.getElementById('imageForArticle')){
       let imageInput = document.getElementById('imageForArticle');
       let backgroundPic = document.getElementsByClassName('SetNewArticle__body-wrapper-form-aside-selectIMG-preview')[0] 
       backgroundPic.style.backgroundImage = 'url('+e.target.currentSrc+')';
       imageInput.value = `${allImages.image[e.target.id]}|${e.target.alt}`
       imageInput.value ? document.getElementsByClassName('panelmultimedia')[0].style.display = 'none': alert('no added') 
     } 
    
   }
  

      let arrayOfImages = [];
      for (let itemsImages = 0; itemsImages < allImages.image.length; itemsImages++) {
      arrayOfImages.push(`${allImages.image[itemsImages]}|${allImages.alt[itemsImages]}`) 
      } 

      console.log("⛹‍️‍♂️ ~ file: panelmultimedia.js ~ line 10 ~ Panelmultimedia ~ langSwitch", langSwitch)
    
        return(
            <> 
           <div className="panelmultimedia">
               <div className="panelmultimedia__wrapper">
                 <button onClick={simpleClossePannel} className="panelmultimedia__wrapper-btn">
                   cerrar
                 </button>
                  <div className="panelmultimedia__wrapper-switch">
                  <button onClick={Switchlang}  className="panelmultimedia__wrapper-switch-firebase switch_firebase">
                   Firebase
                 </button>
                 <button onClick={Switchlang}  className="panelmultimedia__wrapper-switch-php switch_php">
                   php
                 </button>
                   </div>
                   {langSwitch === 'switch_firebase' ? <ul id="switch_firebase"> 
               { 
            arrayOfImages.map((images, e)=>{  
                let imagesSplit = images.split('|');
              return (
                <li  key={e}>
                <Image id={e} onClick={setImageToForm} src={imagesSplit[0]} alt={imagesSplit[1]} width={300} height={180} />
                </li>
                ) 
                      }) //images
                  } 
            </ul>  :  <div id="switch_php">
            <iframe src="https://media.lavour.es/" id="mediaIframe">
            </iframe> 
                 </div> }
                    
            
              
            </div>
            </div>

            </>
        )
    
     
 
    
}

// ===== CALL ALL IMAGES ======= //
 
  
export default Panelmultimedia;