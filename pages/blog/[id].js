import firebase from 'firebase'; 
import {db} from '../../firebase/firebase';
import 'firebase/firestore';
import parse from 'html-react-parser'; 
import Image from 'next/image';
import Head from 'next/head';
import Spacer from '../../components/spacer';
import Link from 'next/link';
import { useEffect , useState} from 'react';
import { useRouter } from 'next/dist/client/router';
import SistemComments from '../../components/articles/SistemComments';
import Listcomments from '../../components/articles/Listcomments';
import { revertFriendlyUrl } from '../../utils/shortText'


let shareFunct = (e) =>{
  console.log(e)
}
function Single({datasingle, id}){ 

const router = useRouter();
   
let copyLink =()=>{   
  const newpos = document.getElementsByClassName('single__wrapper-image-share')[0]
  const setInput = document.createElement('input') 
  setInput.value = window.location.href
   newpos.appendChild(setInput)
  setInput.focus()
  setInput.select()
  try{
   let copied = document.execCommand('copy')
   copied != false ? console.info( "link copied sucessfull"): console.error("no action try copy")
  }catch(err){
  console.log("🚀 ~ somethink goes bad/; " , err) 
  }
 newpos.removeChild(setInput)
}
 
     if(router.isFallback){
       return <pre>Cargando, espera...</pre>
     }

    return(
        <> 
        <Spacer />
        <Head>
          {
             JSON.parse(datasingle).map((single, i)=>{   
               return(
                 
              <header  key={i}>
                <title>{revertFriendlyUrl(single.friendlyUrl)}</title>
                <meta name="description" content={single.title} />
                <meta name="keywords" content={single.keyworld} />
                <meta name="author" content="Ricardo Perez Lavour" />
                <meta name="copyright" content="Ricardo Perez Lavour" />  
                <meta name="author" content="Ricardo Perez Lavour" /> 
                <link rel="icon" href="/favicon.ico" />
                <meta name="theme-color" content="#C90A14"/>

                <meta property="og:image" content={single.image} />
                <meta property="og:image:width" content="1200" />
                <meta property="og:image:height" content="630" />
                <meta property="og:title" content={revertFriendlyUrl(single.friendlyUrl)} />
                <meta property="og:type" content="article" /> 
                <meta property="og:site_name" content="Ricardo Lavour's page" />
                <meta property="og:description" content={single.title} />
                <meta property="og:url" content={`https://lavour.es/${router.locale}/blog/${single.friendlyUrl}`} /> 


                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:site" content="http://lavour.es" />
                <meta name="twitter:title" content={ revertFriendlyUrl(single.friendlyUrl) } />
                <meta name="twitter:description" content={single.title} />
                <meta name="twitter:image" content={single.image} />
                <meta name="twitter:url" content={`lavour.es/${router.locale}/blog/${single.friendlyUrl}`} />
              </header>
               )
               
          })
        }
         
         
        </Head>
         <article className="single">
           
           {
             
             JSON.parse(datasingle).map((single, i)=>{ 
              let htm = parse(single.html);
                 const filterDate = new Date(single.date.seconds*1000)
                 const monts = ['Ene','Feb','Mar','Apr',
                     'May','Jun','Jul','Ago','Sep',
                     'Oct','Nov','Dic'] 

                  
               return(
                 <div key={i} className="single__wrapper">
                <div className="single__wrapper-image-date">
                  <Image src={single.image} alt={single.imageAlt} layout="responsive" width={600} height={300} quality={50}  />
                  <b className="single__wrapper-image-date__date">{`${filterDate.getDate()} ${monts[filterDate.getMonth()]} ${filterDate.getFullYear()}`}</b>
                </div> 
                  <h1>{single.title}</h1>
                  <div className="single__wrapper-image-share">
                    <ul>
                     <li>Share this article...</li> 
                     <li><Image className="share_link" onClick={copyLink} src="/link.svg" alt="share link" height={37} width={37} /></li>
                     <li><Link href={`https://twitter.com/intent/tweet?url=lavour.es/blog/${single.friendlyUrl}`} target="blank" passHref><Image className="share_twt" onClick={shareFunct} src="/twitter.svg" alt="basd" height={37} width={37} /></Link></li>
                     <li><Link href={`https://www.linkedin.com/shareArticle?mini=true&url=lavour.es/blog/${single.friendlyUrl}`} target="blank" passHref><Image className="share_link" onClick={shareFunct} src="/linkedIn.svg" alt="basd" height={37} width={37} /></Link></li>
                     <li><Link href={`https://www.facebook.com/sharer/sharer.php?u=lavour.es/blog/${single.friendlyUrl}`} target="blank" passHref ><Image className="share_face" onClick={shareFunct} src="/facebook.svg" alt="basd" height={37} width={37} /></Link></li>
                    </ul>
                  </div>
                 <div className="single__wrapper-html">{htm}</div>
                 </div>
               )
             })
           }
           <Listcomments id={ id }/>
           <SistemComments id={ id } />
          </article>
          
        </>
    )
}
     
// path
export async function getStaticPaths() { 
  const getdatafire = await firebase.firestore(db).collection('articles').get(); 
  const datafire = getdatafire.docs;   
 let prams = datafire.map(data=>{ 
    return{  params:{ id: data.data().friendlyUrl} }  
  })   
   return{
    paths: prams ,
    fallback: true 
   } 
}
 
 // props 
export async function getStaticProps(context) { 
     let documentID = ''; 
     const dataParams = context.params.id;   
     let data = await firebase.firestore(db).collection('articles').where("friendlyUrl", "==", dataParams).get(); 
         let tata = data.docs; 
        
        let pre = tata.map((fix)=>{  
          
          documentID = fix.id
          return fix.data();   
         })   
      return {
        props: {
          datasingle: JSON.stringify(pre),
          id: documentID
        }
      }
   }
   
 export default Single