import firebase from "firebase";
import { db } from "../../firebase/firebase";
import "firebase/firestore";
import Image from "next/image";
import Head from "next/head";
import Spacer from "../../components/spacer";
import Link from "next/link";
import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { generateText } from "../../utils/shortText";
/*
    http://react-html-parser::: convierte el contenido de un string
    HTML a contenido html normal. Esto porque el texto del post
    viene en formato html convertido en string
    */
let Home = ({ mydata }) => {
  const router = useRouter();
  const [categoryParam, setCategoryParam] = useState();
  const [categorys, setCategorys] = useState(["loading categorys..."]);
  const [langswityc, setLangswityc] = useState(() => {
    return router.locale;
  });

  if (router.events) {
    router.events.on("routeChangeComplete", (url, { shallow }) => {
      // setCategory('MysQL')
      if (router.locale == "es") {
        setLangswityc("es");
      } else {
        setLangswityc("en");
      }
      setCategoryParam(null);
    });
  }

  function data() {
    let whatFech = categoryParam != null ? categoryParam : JSON.parse(mydata);
    return whatFech;
  }
  let setCategory = async (e) => {
    let FindCategory = e.target.innerText.toString();
    let newDataToShow = await firebase
      .firestore(db)
      .collection("articles")
      .where("category", "==", FindCategory)
      .where("lang", "==", langswityc)
      .get();
    let OuputData = newDataToShow.docs;
    let dataSendToShow = OuputData.map((dataTransfer) => {
      return dataTransfer.data();
    });
    setCategoryParam(dataSendToShow);
  };

  useEffect(() => {
    async function fechcategory() {
      let filt = JSON.parse(mydata).map((fechoCategory, e) => {
        return fechoCategory.category;
      });
      // El objeto global Set es una estructura de datos, una colección de valores que permite sólo
      //almacenar valores únicos de cualquier tipo, incluso valores primitivos u referencias a objetos.
      const dataArr = new Set(filt);
      // [...] con esta dependencia creamos una composucion tipo arry y no set
      let result = [...dataArr];
      setCategorys(result);
    }
    fechcategory();
  }, [langswityc]);

  return (
    <>
      <Head>
        <title>Ricardo lavour Blog | Dominican web developer:</title>
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Ricardo E. Lavour" />
        <meta
          property="og:description"
          content="web developer, web master, dominicano, full stack developer, wordpress developer"
        />
        <meta property="og:url" content="https://lavour.es" />
        <meta property="og:image" content="public/blog_Screen.jpg" />
      </Head>
      <Spacer />

      <main className="bodyblog">
        <div className="bodyblog__body">
          <div className="bodyblog__body-wrapper">
            {data().map(function (doc, i) {
              const filterDate = new Date(doc.date.seconds * 1000);
              const monts = [
                "Ene",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Ago",
                "Sep",
                "Oct",
                "Nov",
                "Dic",
              ];
              return (
                <section className="bodyblog__body-wrapper-box" key={i}>
                  <div className="bodyblog__body-wrapper-box-link-box">
                    <Image
                      className="bodyblog__body-wrapper-box-link-box-img"
                      src={doc.image}
                      alt={doc.imageAlt}
                      width={1600}
                      height={630}
                      placeholder="blur"
                      blurDataURL="/lavour_logo.png"
                    />
                  </div>
                  <div className="bodyblog__body-wrapper-box-link-title">
                    <div className="bodyblog__body-wrapper-box-link-title-date">
                      <Image
                        className="bodyblog__body-wrapper-box-link-title-imgCalendar"
                        src="/calendar.svg"
                        alt="ricardo lavour calendar icon"
                        width={20}
                        height={20}
                      />
                      <span>{`${
                        monts[filterDate.getMonth()]
                      }-${filterDate.getDate()}`}</span>
                    </div>
                    <div className="bodyblog__body-wrapper-box-link-title-date-wrapper">
                      <Link href={`/blog/${doc.friendlyUrl}`} passHref>
                        <h3>{generateText(doc.title)}</h3>
                      </Link>
                      <div className="bodyblog__body-wrapper-box-link-title-date-wrapper-info">
                        <span>
                          <b>Category:</b>
                          <p>{doc.category}</p>
                        </span>
                      </div>
                    </div>
                  </div>
                </section>
              );
            })}
          </div>
        </div>
        <aside className="bodyblog__sidebar">
          <div className="bodyblog__sidebar-wrapper">
            <span>Select Category:</span>
            <ul>
              {categorys.map((fechoCategory, e) => {
                return (
                  <li key={e} onClick={setCategory}>
                    {fechoCategory}
                  </li>
                );
              })}
              <li></li>
            </ul>
          </div>
        </aside>
      </main>
    </>
  );
};

export async function getStaticProps({ locale }) {
  console.log("locale=>", locale); // es
  let getDataFromFirebase = await firebase
    .firestore(db)
    .collection("articles")
    .where("lang", "==", `${locale}`)
    .orderBy("date", "desc")
    .get();
  let dataCatchFromFirebase = getDataFromFirebase.docs;
  let objectGet = dataCatchFromFirebase.map((transferDatatoProps) => {
    return transferDatatoProps.data();
  });
  return {
    props: {
      mydata: JSON.stringify(objectGet),
    },
    revalidate: 6000,
  };
}

export default Home;
