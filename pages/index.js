import Spacer from "../components/spacer";
import { useRouter } from "next/router";
import Sliderarticles from "../components/homecomponents/Sliderarticles";
import Selectorarticles from "../components/homecomponents/Selectorarticles";
import { useEffect, useState } from "react";
import Titles from "../components/homecomponents/Titles";
import Printsoftware from "../components/homecomponents/Printsofware";
import WebProducer from "../components/homecomponents/Webproducer";
import Timeline from "../components/homecomponents/Timeline";
import TimelineMobile from "../components/homecomponents/Timelinemobile";
import Portfolios from "../components/homecomponents/Portfolios";
import About from "../components/homecomponents/About";
import Head from "next/head";
// insert linkedin
import LinkedInTag from "react-linkedin-insight";

// const disabled = !user.allowsThirdPartyCookies();
LinkedInTag.init("3976065", "dc", true);
LinkedInTag.track("3976065");

export default function Home() {
  const router = useRouter();

  const [pantalla, setPantalla] = useState("");
  let resize = () => {
    if (document !== "undefined") {
      window.onresize = () => {
        setPantalla(window.innerWidth);
      };
    }
  };

  useEffect(() => {
    setPantalla(window.innerWidth);
    resize();
  }, []);

  return (
    <>
      <Head>
        <title>Ricardo Lavour | Dominican web developer</title>
        <meta
          name="description"
          content="my place for set my portfolios, works, and write about technology topics"
        />
        <meta
          name="keywords"
          content="web developer, web master, dominicano, full stack developer, wordpress developer"
        />
        <meta name="author" content="Ricardo Perez Lavour" />
        <meta name="robots" content="index" />
        <link rel="icon" href="/favicon.ico" />
        <meta name="theme-color" content="#C90A14" />

        <meta property="og:type" content="website" />
        <meta property="og:title" content="Ricardo E. Lavour" />
        <meta
          property="og:description"
          content="web developer, web master, dominicano, full stack developer, wordpress developer"
        />
        <meta property="og:url" content="https://lavour.es" />
        <meta property="og:image" content="./share_img.jpg" />

        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content="Ricardo E. Lavour" />
        <meta
          name="twitter:description"
          content="web developer, web master, dominicano, full stack developer, wordpress developer"
        />
        <meta name="twitter:image" content="./share_img.jpg" />
        <meta name="twitter:url" content="https://lavour.es" />
      </Head>
      <Spacer />
      <div className="home">
        {pantalla >= 550 ? <Selectorarticles /> : <Sliderarticles />}
        <Titles
          texto={router.locale === "en" ? `Graphic Design` : `Diseño Gráfico`}
          posicion="left"
        />
        <Printsoftware />
        <Titles
          texto={router.locale === "en" ? `Web Developer` : `Desarrollo Web`}
        />
        <WebProducer />
        <Titles
          texto={router.locale === "en" ? `jobs TimeLine` : `Últimos empleos`}
          posicion="left"
        />
        {pantalla >= 550 ? <Timeline /> : <TimelineMobile />}
        <Titles
          texto={router.locale === "en" ? `Some Works` : `Algunos trabajos`}
          posicion="right"
        />
        <Portfolios />
        <Titles
          texto={router.locale === "en" ? `About me` : `Sobre mí`}
          posicion="left"
        />
        <About />
      </div>
    </>
  );
}

// export async function getStaticProps() {
//   let fetchs = await fetch('http://localhost:3000/api/type/sliders');
//   let getdata = await fetchs.json();
//   //================================================
//   let fetchsw = await fetch('http://localhost:3000/api/type/works');
//   let miworks = await fetchsw.json();

//   // console.log('sin parameters::',works)
//    return {
//      props: {
//       sliders: getdata,
//       works: miworks
//      },
//    }
// }
