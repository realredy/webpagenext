const sitemap = require("nextjs-sitemap-generator");
const path = require("path");
sitemap({
  alternateUrls: {
    en: "https://lavour.es",
    es: "https://lavour.es/es/",
  },
  baseUrl: "https://lavour.es",
  pagesDirectory: path.resolve(__dirname, "../build/server/pages/"),
  targetDirectory: path.resolve(__dirname, "../public"),
  ignoredPaths: ["404", "500", "/backend", "/blog/[id]", "index"],
  ignoredExtensions: ["mp4", "css", "js", "icon", "svg", "ico"],
  extraPaths: ["/es/blog", "/blog"],
});
