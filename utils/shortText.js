export let generateText = (text) => { 
const idealText = 95
const allText = text.length//texto completo
const calcText = allText > 95 ? parseInt(allText) - parseInt(idealText) : null  
const sliceText =  calcText ? text.slice(0, -calcText ) : null 
const resultText = sliceText ? sliceText.concat('', '...') : text 
return resultText
}

export let revertFriendlyUrl = (url) => {
    const transformUrl = url ? url.replace(/-/g,' ') : 'no have text format here'
    return transformUrl
}
